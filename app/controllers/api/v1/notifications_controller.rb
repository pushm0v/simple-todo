class Api::V1::NotificationsController < Api::V1::BaseController
  @fcm = FcmPusher.new(FIREBASE_SERVER_KEY)

  def single_message(title=nil,message=nil)
    device_token = @authenticate_user.device_token
    unless !device_token.nil? && !message.nil?
      custom_render json_string: error_message("Invalid device token"), status: :unprocessable_entity
      return
    end
    @fcm.send_all([device_token], title, message, { badge: 1, priority: FcmPusher::Priority::HIGH })

    custom_render json_string: "", status: :no_content
  end

  def broadcast_message(title=nil,message=nil)
    device_tokens = []
    users = Users.where(:device_token.nin => ["", nil])
    unless !users.count > 0
      custom_render json_string: error_message("No user found"), status: :unprocessable_entity
      return
    end
    users.each do |user|
      device_tokens << user.device_token
    end

    @fcm.send_all(device_tokens, title, message, { badge: 1, priority: FcmPusher::Priority::HIGH })
    custom_render json_string: "", status: :no_content
  end
end