class Api::V1::AuthController < Api::V1::BaseController
  skip_before_action :verify_token
  before_action :validate_entity, only: [:index]

  def index
    user = Users.where(username: params["username"])

    return unless validate_password(user)

    user = user.first
    @authenticate_user = user

    @authenticate_user = update_device_token

    render json: @authenticate_user.as_json(:only => [:_id, :username, :token, :device_token, :timestamp, :name], :methods => :name), status: :ok
  end

  def validate_entity
    if params["username"].nil? || params["password"].nil?
      render json: { errors: "Username or password value is missing" }, status: :unprocessable_entity
    end
  end

  def validate_password(user)
    if user.count > 0
      password = BCrypt::Password.new(user.first[:password])
      if password == params["password"]
        return true
      end
    end

    render json: { errors: "Invalid username or password" }, status: :unauthorized
    return false
  end
end