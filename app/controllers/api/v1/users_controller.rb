class Api::V1::UsersController < Api::V1::BaseController
  skip_before_action :verify_token
  before_action :validate_entity, only: [:create]

  @user = nil

  def create
    @user.save

    render json: @user.as_json(:only => [:_id, :username, :token, :device_token, :timestamp, :name], :methods => :name), status: :created
  end

  def validate_entity
    if params["password"].nil? || params["password"] == ""
      custom_render json_string: error_message("Password can't be blank"), status: :unprocessable_entity
      return
    end
    hash = hash_password(params["password"])
    token = tokenize(hash)
    device_token = request.headers["X-Device-Token"]
    @user = Users.new({
                          :username => params["username"],
                          :name => params["name"],
                          :password => hash,
                          :token => token,
                          :device_token => device_token,
                          :timestamp => Time.now.to_i
                      })
    if !@user.valid?
      custom_render json_string: error_message(@user.errors), status: :unprocessable_entity
      return
    end

    existing = Users.where(username: @user.username)
    if existing.count > 0
      custom_render json_string: error_message("Username is exist, choose different username"), status: :unprocessable_entity
    end
  end

  private
  def hash_password(password)
    BCrypt::Password.create(password)
  end

  def tokenize(hash)
    payload = {:data => hash}
    JWT.encode payload, nil, 'none'
  end
end