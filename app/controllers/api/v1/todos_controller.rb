class Api::V1::TodosController < Api::V1::BaseController
  before_action :validate_entity, only: [:create]
  before_action :validate_id, only: [:index, :update, :destroy, :show]
  before_action :update_device_token

  @todos = nil
  @todo = nil

  def index
    render json: @todos.as_json, status: :ok
  end

  def show
    render json: @todo.as_json, status: :ok
  end

  def create
    @authenticate_user.todos << @todo
    @authenticate_user.save

    render json: @todo.as_json, status: :created
  end

  def update
    unless params["name"].nil?
      @todo.name = params["name"].strip
    end
    unless params["description"].nil?
      @todo.description = params["description"].strip
    end
    unless params["location_lat"].nil?
      @todo.location_lat = params["location_lat"]
    end
    unless params["location_lon"].nil?
      @todo.location_lon = params["location_lon"]
    end
    unless params["done"].nil?
      @todo.done = params["done"]
    end
    unless params["timestamp"].nil?
      @todo.timestamp = params["timestamp"].to_i
    end
    if !@todo.valid?
      custom_render json_string: error_message(@todo.errors), status: :unprocessable_entity
      return
    end
    @todo.save!

    render json: @todo.as_json, status: :ok
  end

  def destroy
    @todo.delete
    render json: nil, status: :no_content
  end

  def validate_entity

    params["location_lat"] ||= 0.0
    params["location_lon"] ||= 0.0
    params["done"] ||= false
    @todo = Todos.new(
{
        :name => params["name"],
        :description => params["description"],
        :timestamp => Time.now.to_i,
        :done => params["done"],
        :location_lat => params["location_lat"],
        :location_lon => params["location_lon"],
        :users_id => @authenticate_user.id
      }
    )

    if !@todo.valid?
      custom_render json_string: error_message(@todo.errors), status: :unprocessable_entity
    end
  end

  def validate_id
    id = params["id"]
    @todos = @authenticate_user.todos
    if !id.nil?
      @todo = @todos.where(id: id).first
      if @todo.nil?
        custom_render json_string: error_message("Invalid todo"), status: :not_found
      end
    end
  end
end