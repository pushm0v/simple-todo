class Api::BaseController < BaseController
  before_action :verify_token

  @authenticate_user = nil

  def verify_token
    token = request.headers["Authorization"]
    user = Users.where(token: token)

    if user.count == 0
      render json: { errors: "Invalid token" }, status: :unauthorized
      return
    end

    @authenticate_user = user.first

  end

  def update_device_token
    device_token = request.headers["X-Device-Token"]
    if device_token != "" && !device_token.nil?
      @authenticate_user.device_token = device_token
      @authenticate_user.save
    end
    @authenticate_user
  end
end
