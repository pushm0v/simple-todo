class Api::PingsController < BaseController
  def show
    render json: "OK", status: :ok
  end
end
