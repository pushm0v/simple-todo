class BaseController < ApplicationController
  
  def raise_routing_error
    raise ActionController::RoutingError.new(params[:path])
  end

  def user_locale
    request.headers['Accept-Language']
  end

  private

  def custom_render(json_string: , status:)
    self.status = status
    self.content_type = 'application/json'
    self.headers['Content-Length'] = json_string.present? ? json_string.bytesize.to_s : '0'.freeze
    self.response_body = json_string.present? ? json_string : ''.freeze
  end

  def error_message(errors)
    msg = ""

    if errors.is_a? (String)
      msg = errors
    else
      msg = errors.full_messages.join(', ')
    end
    { errors: msg }.to_json
  end
end
