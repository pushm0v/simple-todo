class Todos
  include Mongoid::Document

  validates :location_lat , numericality: { greater_than_or_equal_to:  -90, less_than_or_equal_to:  90 }, presence: true
  validates :location_lon, numericality: { greater_than_or_equal_to: -180, less_than_or_equal_to: 180 }, presence: true
  validates :name, presence: true
  validates :description, presence: true

  belongs_to :users, class_name: 'Users', inverse_of: :todos
  field :timestamp, type: Integer
  field :name, type: String
  field :description, type: String
  field :done, type: Boolean
  field :location_lat, type: Float
  field :location_lon, type: Float

  def location
    { lat: self.location_lat, lon: self.location_lon }
  end
end