class Users
  include Mongoid::Document

  validates :username, presence: true
  validates :password, presence: true

  field :timestamp, type: Integer
  field :name, type: String
  field :username, type: String
  field :password, type: String
  field :token, type: String
  field :device_token, type: String

  has_many :todos, class_name: 'Todos', inverse_of: :users, autosave: true
end