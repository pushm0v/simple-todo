Rails.application.routes.draw do
  get 'ping', to: 'api/pings#show', defaults: { format: :json }
  post 'api/v1/auth', to: 'api/v1/auth#index', defaults: { format: :json }
  post 'api/v1/notifications/single_message', to: 'api/v1/notifications#single_message', defaults: { format: :json }
  post 'api/v1/notifications/broadcast_message', to: 'api/v1/notifications#broadcast_message', defaults: { format: :json }
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :todos, :only => [:index, :create, :update, :destroy, :show]
      resources :users, :only => [:create]
    end
  end

  match "*path", to: "base#raise_routing_error", via: :all
end
