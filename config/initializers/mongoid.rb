require 'mongoid'

Mongoid.raise_not_found_error = false
module Mongoid
  module Document
    def as_json(options={})
      attrs = super(options)
      attrs.each_with_object({}) { |(k,v),g|
        if v.class == BSON::ObjectId
          attrs[k] = v.to_s
        end
      }
      attrs["id"] = attrs["_id"].to_s
      attrs.delete("_id")
      attrs
    end
  end
end