# Simple Todo

As a part of iOS Training, this RESTful app will do user registration, authentication, & Todo CRUD

## How To

- Install mongoDB `brew install mongodb`
- Create ENV Var `MONGO_URL` with value `mongodb://localhost:27017`
- `bundle install`
- `bundle exec rails s`


### Headers
Any protected URL will required `token` as authorization flow, if token is missing or invalid will return `401 Unauthorized`.
`X-Device-Token` header required for `Firebase Cloud Messaging` purpose.
- `Authorization` : `token`
- `X-Device-Token` : `device_token`

Unprotected URL :
- `HOST/api/v1/users`
- `HOST/api/v1/auth`
- `HOST/ping`

### New user registration

- `POST HOST/api/v1/users`
    - payload :
    ```ruby
    {
        "username": "test",
        "password": "test",
        "name": "Some test user"
    }
    ```
    - response : User model
    - status code : 201

### User authentication

- `POST HOST/api/v1/auth`
    - payload :
    ```ruby
    {
        "username": "test",
        "password": "test"
    }
    ```
    - response : Token for authorization
    - status code : 200
    - status code failed: 401

### Todo CRUD

- `GET HOST/api/v1/todos`
    - payload : nil
    - response : Array of todos
    - status code : 200

- `GET HOST/api/v1/todos`
    - payload : nil
    - response : Array of todos
    - status code : 200

- `GET HOST/api/v1/todos/:id_todo`
    - payload : nil
    - response : Todo model
    - status code : 200

- `POST HOST/api/v1/todos`
    - payload :
    ```ruby
    {
    	"name": "New todo",
    	"description": "New todo",
    	"location_lat": 1.0,
    	"location_lon": 1.0,
    }
    ```
    - response : Todo model
    - `location_lat` & `location_lon` if not set will be `0.0` by default
    - status code : 201

- `PUT HOST/api/v1/todos/:id_todo`
    - payload :
    ```ruby
        {
        	"name": "Updated todo",
        	"description": "Updated todo",
        	"location_lat": 2.0,
        	"location_lon": 2.0,
        }
    ```
    - response : Todo model
    - status code : 200

- `DELETE HOST/api/v1/todos/:id_todo`
    - payload : nil
    - response : nil
    - status code : 204